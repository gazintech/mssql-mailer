# Introduction

`mssql-mailer` is a thin wrapper around
[*MSSQL Database Mail*](https://docs.microsoft.com/en-us/sql/relational-databases/database-mail/database-mail)
that adds the following functionality:

1.  minimal footprint—the whole system consists of one table, one
    procedure, and one job.
2.  fire-and-forget operation—the user need only enque an e-mail, and
    the system will take over and handle it automatically.
3.  sensible error-recovery—the system will retry failed send attempts
    with exponentially increasing intervals.
4.  single point of monitoring—the actual status of queued e-mail is
    available in the queue table, along with an error message if
    appropriate.

# Installation

1.  Open `gtmailer.sql`.
2.  Review the default parameters specified at the beginning of the
    script and change them to your requirements.
3.  Run the script against the database where you want install the
    system.
4.  Start the job `GT_Mailer`.

Make sure a *DBMail* profile with the name specified in `@profile`
is
[created](https://docs.microsoft.com/en-us/sql/relational-databases/database-mail/create-a-database-mail-profile#SSMSProcedure)
and has makes but a single
[attempt](https://docs.microsoft.com/en-us/sql/relational-databases/database-mail/configure-database-mail#SystemParameters)
of sending e-mail, in order not to
interfere with the system's recovery mechanism.

# Usage

The script will have created stored procedure `GT_SendMail`, which is to
be used for sending e-mail.
Its parameters are self-explanatory.

# Administration

The mail queue is stored in table `GT_MailerQueue`.
In addition to self-explanatory columns with e-mail data, it has four
columns describing the state of each entry:

`MailId`  
is the id of the mail item in the *DBMail* internal queue.

`LastUpdate`  
is the date and time of the last operation on the
entry. 

`Attempts`  
is the number of attempts already made to send the e-mail.

`Status`  
encodes the current status of the entry.

`ErrorMsg`  
contains the error message of the last operation.

`Status` can assume the following values:

`New`  
entry has been added to the mailer queue but not been processed.

`Queued`  
entry has been sent do *MSSQL DBMail*.

`Error:queue`  
*MSSQL DBMail* failed to accept the message.

`Error:mail`  
*MSSQL DBMail* failed to send the message.

`OK`  
e-mail has been sent.

Both error statuses are not always final: the system will try keep
trying to send those entries until all attempts are exhausted.

The system automatically cleans up data about successfully sent e-mails
over a month old and about failed deliveries over three months old.
