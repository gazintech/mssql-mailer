/* Copyright 2022                GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */

DECLARE	@profile     VARCHAR( 255 ) =  'test'; -- DB Mail profile name.
DECLARE	@maxAttempts INT            =       8; -- Maximum number of attemps.
DECLARE	@delayMin    INT            =       5; -- Base delay in minutes.
DECLARE	@exp_base    INT            =       2; -- Multiple for delay next attempt.

CREATE TABLE ##GT_MailerSettings
(	MailProfile VARCHAR( 255 ),
	MaxAttempts INT,
	NumPower    INT,
	BaseDelay   INT
)
INSERT INTO ##GT_MailerSettings VALUES ( @profile, @maxAttempts, @exp_base, @delayMin );

GO
IF OBJECT_ID( N'GT_MailerQueue', N'U' ) IS NOT NULL
	DROP TABLE GT_MailerQueue
GO
CREATE TABLE GT_MailerQueue
(	Id         INT PRIMARY KEY IDENTITY,
	MailId     INT,
	LastUpdate DATETIME,
	Attempts   INT,
	-- Mail statuses in the queue:
	-- 
	-- New         - ready to send
	-- Queued      - mail is queued for sending
	-- Error:queue - mail sending error
	-- Error:mail  - error in mail queue	
	-- OK          - sending success
	Status     VARCHAR (  20 ),
	ErrorMsg   NVARCHAR( MAX ),
	Subject    NVARCHAR( 255 ),
	Body       NVARCHAR( MAX ),
	BodyFormat VARCHAR (  20 ),
	Recipients VARCHAR ( MAX )
)

IF Object_ID( 'GT_SendMail' ) IS NOT NULL
	DROP PROCEDURE GT_SendMail

GO
CREATE PROCEDURE GT_SendMail
(	@subject    NVARCHAR( 255 ), -- Here is the subject of the email.
	@body       NVARCHAR( MAX ), -- The body of the email in text or html format.
	@isHtml     BIT,             -- Format indicator: 1 is html, 0 is plain text.
	@recipients VARCHAR ( MAX )  -- A string with recipient addresses, 
	                             -- separated by a semicolon.
)
/* Copyright 2022                GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */
/*                                                                            */
/* This project in Gitlab: https://gitlab.com/gazintech/mssql-mailer          */
AS
-- TODO: pass query for recepients.
BEGIN
	DECLARE @bodyFormat VARCHAR( 20 )

	IF @isHtml = 1 SET @bodyFormat = 'HTML'
	ELSE           SET @bodyFormat = 'TEXT'

	INSERT INTO GT_MailerQueue
	(	MailId , LastUpdate, Attempts,
		Status , ErrorMsg  ,
		Subject, Body      , BodyFormat, Recipients
	)
	VALUES 
	(	NULL    ,   GETDATE(), 0, 
		'New'   ,   NULL     ,
		@subject,   @body    , @bodyFormat,   @recipients
	);
END;

GO
IF EXISTS( SELECT * FROM msdb.dbo.sysjobs WHERE name = N'GT_Mailer' )
	EXEC msdb.dbo.sp_delete_job @job_name = N'GT_Mailer';

IF EXISTS( SELECT * FROM msdb.dbo.sysschedules WHERE name = N'GT_MailProccess' )
	EXEC msdb.dbo.sp_delete_schedule @schedule_name = N'GT_MailProccess';

EXEC msdb.dbo.sp_add_job
	@job_name    = N'GT_Mailer',
	@enabled     = 1,
	@description = 'See license in Gitlab: https://gitlab.com/gazintech/mssql-mailer';

GO
DECLARE @profile     NVARCHAR( 255 )
DECLARE @maxAttempts INT
DECLARE @exp_base    INT
DECLARE @delayMin    INT
DECLARE @dbName      VARCHAR( 255 )

SELECT 
	@profile     = MailProfile,
	@maxAttempts = MaxAttempts,
	@exp_base    = NumPower,
	@delayMin    = BaseDelay,
	@dbName      = DB_NAME()
FROM ##GT_MailerSettings;

DECLARE @cr NVARCHAR( 2 ) = '
';
DECLARE @cmdArgs NVARCHAR( MAX ) =
	N'DECLARE @profile     VARCHAR( 255 ) = ''' + @profile + ''''                         + @cr +
	N'DECLARE @maxAttempts INT            = '   + CAST( @maxAttempts AS NVARCHAR( 100 ) ) + @cr +
	N'DECLARE @exp_base    INT            = '   + CAST( @exp_base    AS NVARCHAR( 100 ) ) + @cr +
	N'DECLARE @delayMin    INT            = '   + CAST( @delayMin    AS NVARCHAR( 100 ) ) + @cr;
DECLARE @cmd NVARCHAR ( MAX ) = @cmdArgs +
N'	DECLARE @curDate     DATETIME       = GETDATE()
	DECLARE @id          INT
	DECLARE @mailId      INT
	DECLARE @lastUpdate  DATETIME
	DECLARE @attempts    INT
	DECLARE @recipients  VARCHAR ( MAX )
	DECLARE @subject     NVARCHAR( 255 )
	DECLARE @body        NVARCHAR( MAX )
	DECLARE @bodyFormat  VARCHAR (  20 )
	DECLARE @lastModDate DATETIME
	DECLARE @sentStatus  VARCHAR (   8 )
	DECLARE @isHtml      BIT
	DECLARE @status      VARCHAR (  20 )
	DECLARE @errorMsg    NVARCHAR( MAX )
	DECLARE @eventType   VARCHAR (  11 )
	DECLARE @description NVARCHAR( MAX )

	DECLARE MailCursor CURSOR STATIC FOR
		SELECT
			Id      , MailId , LastUpdate,
			Attempts, Status , ErrorMsg  ,
			Subject , Body   , BodyFormat, Recipients
		FROM  GT_MailerQueue
		WHERE 
			Attempts < @maxAttempts AND 
			(	Status = ''New'' OR 
				(	Status LIKE ''Error:%'' AND
						DATEDIFF( MI, LastUpdate, @curDate ) >=
						@delayMin * POWER( @exp_base, @attempts ) 
				)
			)

	-- 1. Call sp_send_dbmail
	OPEN MailCursor;
	WHILE ( 1 = 1 )
	BEGIN
		FETCH NEXT FROM MailCursor 
		INTO
			@id      , @mailId, @lastUpdate, 
			@attempts, @status,	@errorMsg  , 
			@subject , @body  , @bodyFormat, @recipients

		IF ( @@FETCH_STATUS <> 0 ) BREAK
	
		SET @status = ''Queued''
		SET @errorMsg  = NULL
			
		BEGIN TRY
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = @profile,
				@recipients   = @recipients,
				@body         = @body,
				@subject      = @subject,
				@body_format  = @bodyFormat,
				@mailitem_id  = @mailId OUTPUT;
		END TRY
		BEGIN CATCH
			SET @status   = ''Error:mail''
			SET @errorMsg = ERROR_MESSAGE();
		END CATCH

		UPDATE GT_MailerQueue
		SET 
			MailId     = @mailId,
			Attempts   = @attempts + 1,
			LastUpdate = @curDate,
			Status     = @status,
			ErrorMsg   = @errorMsg
		WHERE
			Id = @id

	END
	CLOSE MailCursor
	DEALLOCATE MailCursor

	-- 2. Update statuses

	UPDATE GT_MailerQueue
	SET 
		Status     = 
			CASE Items.Status
				WHEN ''sent''   THEN ''OK''
				WHEN ''failed'' THEN ''Error:queue''
				ELSE NULL
			END,
		ErrorMsg   = 
			CASE Items.Status
				WHEN ''sent''   THEN NULL
				WHEN ''failed'' THEN Items.Descript
				ELSE NULL
			END,
		LastUpdate = Items.LastModDate
	FROM 
	(	SELECT 
			AllItems.mailitem_id   AS MailId, 
			AllItems.last_mod_date AS LastModDate, 
			EventLog.Msg           AS Descript,
			AllItems.sent_status   AS Status
		FROM msdb.dbo.sysmail_allitems  AS AllItems
		LEFT JOIN 
		(	SELECT EL.mailitem_id, EL.event_type + '': '' + EL.description Msg
			FROM
			(	SELECT mailitem_id, MAX( log_id ) log_id
				FROM msdb.dbo.sysmail_event_log
				GROUP BY mailitem_id
			) LastEvent
			JOIN msdb.dbo.sysmail_event_log EL 
			ON   EL.log_id = LastEvent.log_id
		) AS EventLog ON 
			AllItems.mailitem_id = EventLog.mailitem_id
		WHERE 
			AllItems.sent_status = ''failed'' OR
			AllItems.sent_status = ''sent''
	) AS Items
	WHERE
		Items.MailId = GT_MailerQueue.MailId
';

EXEC msdb.dbo.sp_add_jobstep
	@job_name          = N'GT_Mailer',
	@step_name         = N'Process queue',
	@subsystem         = N'TSQL',
	@command           = @cmd,
	@retry_attempts    = 0,
	@retry_interval    = 0,
	@on_success_action = 3,
	@on_fail_action    = 2,
	@database_name     = @dbName;

GO
DECLARE @maxAttempts INT
DECLARE @dbName      VARCHAR( 255 )

SELECT 
	@maxAttempts = MaxAttempts, 
	@dbName      = DB_NAME() 
FROM ##GT_MailerSettings;

DECLARE @cr NVARCHAR( 2 ) = '
';
DECLARE @cmdClearMq NVARCHAR( MAX ) =
N'	DELETE FROM GT_MailerQueue 
	WHERE 
		Status <> ''OK''       AND 
		LastUpdate < @beforeForFailed AND
		Attempts <> ' + CAST( @maxAttempts AS NVARCHAR( 100 ) ) + @cr;
	
DECLARE @cmd NVARCHAR( MAX ) = 
N'	DECLARE @curDate          DATETIME = GETDATE()
	DECLARE @beforeForFailed  DATETIME
	DECLARE @beforeForSuccess DATETIME

	SET @beforeForFailed  = DATEADD( month, -3, @curDate )
	SET @beforeForSuccess = DATEADD( month, -1, @curDate )

	EXECUTE msdb.dbo.sysmail_delete_mailitems_sp 
		@sent_before = @beforeForFailed, 
		@sent_status = ''failed'';

	EXECUTE msdb.dbo.sysmail_delete_mailitems_sp 
		@sent_before = @beforeForSuccess, 
		@sent_status = ''sent'';

	EXECUTE msdb.dbo.sysmail_delete_log_sp 
		@logged_before = @beforeForFailed,     
		@event_type = ''error'';

	EXECUTE msdb.dbo.sysmail_delete_log_sp 
		@logged_before = @beforeForFailed,     
		@event_type = ''warning'';

	EXECUTE msdb.dbo.sysmail_delete_log_sp 
		@logged_before = @beforeForSuccess, 
		@event_type = ''information'';

	EXECUTE msdb.dbo.sysmail_delete_log_sp 
		@logged_before = @beforeForSuccess, 
		@event_type = ''success'';

	DELETE FROM GT_MailerQueue WHERE Status = ''OK'' AND LastUpdate < @beforeForSuccess;
    ' + @cmdClearMq;

EXEC msdb.dbo.sp_add_jobstep
	@job_name          = N'GT_Mailer',
	@step_name         = N'Cleanup',
	@subsystem         = N'TSQL',
	@command           = @cmd,
	@retry_attempts    = 0,
	@retry_interval    = 0,
	@on_success_action = 1,
	@on_fail_action    = 2,
	@database_name     = @dbName;

GO
DECLARE @intervalMin INT

SELECT @intervalMin = BaseDelay 
FROM   ##GT_MailerSettings;

EXEC msdb.dbo.sp_add_schedule
	@schedule_name        = N'GT_MailProccess',
	@enabled              = 1,
	@freq_type            = 4,
	@freq_interval        = 1,
	@freq_subday_type     = 0x4,
	@freq_subday_interval = @intervalMin,
	@active_start_time    = NULL;

GO
EXEC msdb.dbo.sp_attach_schedule  
	@job_name      = N'GT_Mailer',  
	@schedule_name = N'GT_MailProccess'; 

GO
EXEC msdb.dbo.sp_add_jobserver  
	@job_name = N'GT_Mailer';

GO

DROP TABLE ##GT_MailerSettings
GRANT EXEC ON dbo.GT_SendMail TO PUBLIC
